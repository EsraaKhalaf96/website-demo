$(document).ready(function () {
  // $('.carousel.carousel-slider').carousel({ fullWidth: true });
  $('.carousel.carousel-slider').carousel({
    fullWidth: true,
    indicators: true
  });
  $(".button-collapse").sideNav();


  $('#mainMenu').click(function () {
    //when click open menu:-
    // remove class open -- add class close
    //Change open word to close 
    // if ($(".toggle-menu").hasClass("open")) {
    //   $(this).find('.open').removeClass('open').addClass('close');
    //   $(".menuItem").animate({
    //     width: 'toggle'
    //   }, 350);
    // } else {
    //   $(this).find('.close').removeClass('close').addClass('open');
    //   $(".menuItem").animate({
    //     width: 'toggle'
    //   }, 350);
    // }
  })
  $("#menutoggel").click(function(){
    
    $("#panel").slideToggle('slow');
    $(".close").click(function(){
      $("#panel").hide('slow');
    })

  });

  $("#collaps").click(function(){
    $("#form").toggle('slow');
  });
  $('.indicator-item.active').hover(function(){
    $('.tooltip .tooltiptext').removeClass('hidden');
  }, function() {
    $('.tooltip .tooltiptext').addClass('hidden');
  })

  $(".carousel .indicators .indicator-item").css("position", "relative");
  $(".carousel .indicators .indicator-item").hover( function()
  {
    // $('.tooltip .tooltiptext').css({"visibility" :"visible" , "opacity": "1"});
  })
  $(window).scroll(function(){
  	var scroll = $(window).scrollTop();
	  if (scroll > 200) {
      $(".scrolltoWhite").css( {"background" : "white" , "box-shadow": "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)"});
      $("nav .button-collapse i").css("color" , "black");
	  }

	  else{
      $(".scrolltoWhite").css({"background" :"transparent" ,"box-shadow": "none"});  	
      $("nav .button-collapse i").css("color" , "white");
	  }
  })

 

});

function initMap() {
  // The location of Uluru
  var uluru = {lat: 26.820553, lng: 30.802498};
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 4, center: uluru});
 
  var marker = new google.maps.Marker({position: uluru, map: map});
}